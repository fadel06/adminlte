<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;

class PertanyaanController extends Controller
{
    public function create(){
      return view('pertanyaan.create');
    }

    public function store(Request $request){
      // dd($request->all());
      $request->validate([
        'judul' => 'required|unique:questions',
        'isi' => 'required'
      ]);
      // $query = DB::table('questions')->insert([
      //   'judul' => $request['judul'],
      //   'isi' => $request['isi']
      // ]);

      // $question = new Question;
      // $question->judul = $request['judul'];
      // $question->isi = $request['isi'];
      // $question->save();

      $question = Question::create([
        'judul' => $request['judul'],
        'isi' => $request['isi']
      ]);

      return redirect('/pertanyaan')->with('success', 'Pertanyaan Terkirim');
    }

    public function index(){
      // $pertanyaan = DB::table('questions')->get();
      $questions = Question::all();
      // dd($questions);
      return view('pertanyaan.index', compact('questions'));
    }

    public function show($id){
      // $no = DB::table('questions')->where('id', $id)->first();
      $no = Question::find($id);
      // dd($tanya);
      return view('pertanyaan.show', compact('no'));
    }

    public function edit($id){
      // $no = DB::table('questions')->where('id', $id)->first();
      $no = Question::find($id);
      return view('pertanyaan.edit', compact('no'));
    }

    public function update($id, Request $request){
      $request->validate([
        'judul' => 'required|unique:questions',
        'isi' => 'required'
      ]);
      // $query = DB::table('questions')
      //              ->where('id', $id)
      //              ->update([
      //                 'judul' => $request['judul'],
      //                 'isi' => $request['isi']
      //              ]);
      $update = Question::where('id', $id)->update([
        'judul' => $request['judul'],
        'isi' => $request['isi']
      ]);

      return redirect('/pertanyaan')->with('success', 'Berhasil edit pertanyaan!');
    }

    public function destroy($id){
      // $query = DB::table('questions')->where('id', $id)->delete();
      Question::destroy($id);
      return redirect('/pertanyaan')->with('success', 'Berhasil hapus pertanyaan!');
    }
}

@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Pertanyaan {{$no->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <!-- <form role="form" action="/pertanyaan/{{$no->id}}" method="POST"> -->
    <form role="form" action="{{ route('pertanyaan.update', ['pertanyaan' => $no->id ])}}" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="title">Judul</label>
          <input type="text" class="form-control" id="title" name="judul" placeholder="Masukkan Judul" value=" {{ old('judul', $no->judul)}}">
          @error('judul')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="content">Pertanyaan</label>
          <input type="text" class="form-control" id="content" name="isi" value="{{ old('isi', $no->isi )}}" placeholder="Masukkan Pertanyaan Anda">
          @error('isi')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
</div>
@endsection

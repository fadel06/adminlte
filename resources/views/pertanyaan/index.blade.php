@extends('adminlte.master')

@section('content')
  <div class="mt-3 ml-3">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tabel Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
          <div class="alert alert-success">
            {{ session('success')}}
          </div>
        @endif
        <!-- <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan Baru</a> -->
        <a class="btn btn-primary mb-2" href="{{ route('pertanyaan.create')}}">Buat Pertanyaan Baru</a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">No</th>
              <th>Judul</th>
              <th>Isi</th>
              <th style="width: 40px">Action</th>
            </tr>
          </thead>
          <tbody>
            @forelse($questions as $key => $question)
              <tr>
                <td>{{ $key + 1}}</td>
                <td>{{ $question->judul}} </td>
                <td>{{ $question->isi}} </td>
                <td style="display: flex;">
                  <!-- <a href="/pertanyaan/{{$question->id}}" class="btn btn-info btn-sm">show</a> -->
                  <a href="{{ route('pertanyaan.show', ['pertanyaan' => $question->id ])}}" class="btn btn-info btn-sm">show</a>
                  <!-- <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-warning btn-sm ml-2">edit</a> -->
                  <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $question->id ])}}" class="btn btn-warning btn-sm ml-2">edit</a>
                  <!-- <form action="/pertanyaan/{{$question->id}}" method="post"> -->
                  <form action="{{ route('pertanyaan.destroy', ['pertanyaan' => $question->id ])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="4" align="center"> No Data </td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
@endsection
